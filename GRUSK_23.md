# GRUSK - 2023

A team of 7 members from the club joined the event. Left Morgantown 1900 h and we arrived at the venue 2330 h.

Members joined

- N0IAN
- WV8EHB
- W8TES
- KB8CFD
- N3AMK
- KA3WVU
- KE8TJE

## Even details

- Operations started with a meeting ~0810 h with organizer (Travis) and locations of the stations were communicated and rides were found to radio operators with different deployement times.
- We had primary/secondary communication channels allocated as follows
	- 146.555 Mhz
	- 146.450 Mhz
	- Spruce knob repeater: 147.285MHz (+) PL 103.5Hz

- stations were established at the designated locations and N3AMK ran the net which was located on the summit
	- N3AMK insert comments from the logs


	- Aid station at Whitmore: KE8TJE
		- Started operation at 1140 h 
		- Power failier of a Lipo cell. may be due to heating
		- Repeater contact was made with WV8EHB. and Netcontrol was later contacted
		-	ended up getting riders with mechanical faliers and this was relayed to net via the repeater. (Transport was requested since from what information was available throwgh the aid station crew that was the standard practice.)
		- Packed up the aid station and left.Nortified net about this. (Direct comms was established with a wip via the truck)


## Proposed changes

- The meeting with the organization needs to be more in detail and may be work sheet can be developed on key points all participating operators know about the backend operation of the bike race. (updated as of latest year changes from the org commite.
- start the net from base station until the summit is up to get information out to stations that a riding long distances.
	- the net formaly started 1100 h. we didn't miss any updates since riders will not reach aid stations untill 2-3 h from the start of the race. however this could be done earlier.
- Have accurate travel estimates based on local knowledge.
- number aid stations since names are forign to the operators. (eg aid x instead of location)  
- Possibility of getting a GMRS license to the club and hand over radios to the following
	- timing station
  - Organizors ( not only Travis, There are key members mostly relatives and friends of Travis who has done it for >5 year (eg. Jason,Heath,Steve,Jan)
 - Place a ham at base with a cross band repeate capable radio. 

## Issues seen
	
- changes made to the race back end operations were not propergated to aid stations/operators prior to being deployed
- aid station crews made aware about the course of actions needed. Make it clear they have control over the information sent over radio. The radio operator is there to facilitate information flow.
	-	lack of an actual control body from the organization side. In earlier years there was a hard cut off track close time.
- common information rides asked for and no one knew;
	- how far is the next aid
	- how long are we 


## Changes to the go kit

- Include slim jims for ease of use
- add a mini voltmeter/ find from the radio
- add a quick reference functions in the kit. Manual is there but it takes time to find what is needed.
- include a multi-tool to the kit
