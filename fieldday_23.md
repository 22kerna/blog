# ARRL Field day: 2023

## Participants

- KE8TJE
- N3AMK
- N0IAN
- KA3WVU
- N8MDC 

## Equipment 

- FT 891
- EFHW
- 40 Dipole antenna
- ICOM 7300 (KA3WVU)
- Arrow 2

## Comments

- 40 m was the best band to operate. A combined total of > 300 contacts were completed during the 24 h period by the club members
- a couple of sat contacts were attempted but we couldn't complete a QSO
- modifications to the Generator grounding was done on site.
- FT891 showed splattering to other bands during overnight operation. This may be attributed to closely spaced antennas.(needs testing)
- The EFHW was good on 10/20 m and we had decent contacts


## Improvements and modification

- Try digital modes during the whole period. we need to cut a new 40 m wire for the EFHW. This should allow simultanious operation even when other stations are using phone bands.
- find a TNC for Rtty/PSK31 and any other digital modes

Some pictures:

<img src="./pics/fd_23_1.jpg" width="30%" />
<img src="./pics/fd_23_2.jpg" width="30%" />
<img src="./pics/fd_23_3.jpg" width="30%" />
